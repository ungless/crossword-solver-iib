\contentsline {chapter}{\numberline {1}Introduction}{3}{chapter.1}%
\contentsline {section}{\numberline {1.1}Ambiguity resolution}{3}{section.1.1}%
\contentsline {section}{\numberline {1.2}Existing work}{4}{section.1.2}%
\contentsline {section}{\numberline {1.3}Hypotheses and experimental outline}{4}{section.1.3}%
\contentsline {chapter}{\numberline {2}Background}{6}{chapter.2}%
\contentsline {section}{\numberline {2.1}Historical background}{6}{section.2.1}%
\contentsline {section}{\numberline {2.2}Why are cryptic crossword clues a challenge for NLP?}{8}{section.2.2}%
\contentsline {section}{\numberline {2.3}Taxonomy of clue-solving skills}{10}{section.2.3}%
\contentsline {subsection}{\numberline {2.3.1}Hypernymy and hyponymy}{10}{subsection.2.3.1}%
\contentsline {subsection}{\numberline {2.3.2}Homography}{10}{subsection.2.3.2}%
\contentsline {section}{\numberline {2.4}Summary}{11}{section.2.4}%
\contentsline {chapter}{\numberline {3}Method}{12}{chapter.3}%
\contentsline {section}{\numberline {3.1}Datasets}{12}{section.3.1}%
\contentsline {subsection}{\numberline {3.1.1}Cryptonite}{12}{subsection.3.1.1}%
\contentsline {subsection}{\numberline {3.1.2}Enhanced Cryptonite test set}{12}{subsection.3.1.2}%
\contentsline {subsection}{\numberline {3.1.3}Annotated dataset}{13}{subsection.3.1.3}%
\contentsline {section}{\numberline {3.2}Statistics for datasets}{14}{section.3.2}%
\contentsline {section}{\numberline {3.3}Dataset limitations}{14}{section.3.3}%
\contentsline {section}{\numberline {3.4}Models}{15}{section.3.4}%
\contentsline {subsection}{\numberline {3.4.1}T5-Large}{15}{subsection.3.4.1}%
\contentsline {subsection}{\numberline {3.4.2}T5-3B}{15}{subsection.3.4.2}%
\contentsline {subsection}{\numberline {3.4.3}ByT5-Large}{15}{subsection.3.4.3}%
\contentsline {subsection}{\numberline {3.4.4}ByT5-XL}{15}{subsection.3.4.4}%
\contentsline {section}{\numberline {3.5}Overall architecture}{16}{section.3.5}%
\contentsline {subsection}{\numberline {3.5.1}Tokenizers}{16}{subsection.3.5.1}%
\contentsline {subsubsection}{SentencePiece}{16}{section*.2}%
\contentsline {subsubsection}{ByT5}{16}{section*.3}%
\contentsline {section}{\numberline {3.6}Libraries}{17}{section.3.6}%
\contentsline {section}{\numberline {3.7}Training protocol}{17}{section.3.7}%
\contentsline {section}{\numberline {3.8}Evaluation metrics}{18}{section.3.8}%
\contentsline {subsection}{\numberline {3.8.1}Accuracy}{18}{subsection.3.8.1}%
\contentsline {subsection}{\numberline {3.8.2}Enumeration accuracy}{18}{subsection.3.8.2}%
\contentsline {subsection}{\numberline {3.8.3}Edit distance}{18}{subsection.3.8.3}%
\contentsline {subsection}{\numberline {3.8.4}Semantic similarity}{19}{subsection.3.8.4}%
\contentsline {chapter}{\numberline {4}Results}{20}{chapter.4}%
\contentsline {section}{\numberline {4.1}Models}{20}{section.4.1}%
\contentsline {subsection}{\numberline {4.1.1}Baseline (T5-Large unannotated)}{20}{subsection.4.1.1}%
\contentsline {subsection}{\numberline {4.1.2}T5-3B unannotated}{22}{subsection.4.1.2}%
\contentsline {subsection}{\numberline {4.1.3}ByT5-Large unannotated}{24}{subsection.4.1.3}%
\contentsline {subsection}{\numberline {4.1.4}ByT5-XL unannotated}{26}{subsection.4.1.4}%
\contentsline {subsection}{\numberline {4.1.5}T5-Large annotated}{27}{subsection.4.1.5}%
\contentsline {subsection}{\numberline {4.1.6}T5-3B annotated}{30}{subsection.4.1.6}%
\contentsline {section}{\numberline {4.2}Overall comparison}{32}{section.4.2}%
\contentsline {subsection}{\numberline {4.2.1}Clue types}{32}{subsection.4.2.1}%
\contentsline {subsubsection}{Anagrams}{32}{section*.4}%
\contentsline {subsubsection}{Charades}{34}{section*.5}%
\contentsline {subsubsection}{Containments}{35}{section*.6}%
\contentsline {subsection}{\numberline {4.2.2}Single- vs. multi-word}{36}{subsection.4.2.2}%
\contentsline {subsection}{\numberline {4.2.3}Effect of perceived ease of puzzle}{37}{subsection.4.2.3}%
\contentsline {subsection}{\numberline {4.2.4}Subjective analysis of explanations}{38}{subsection.4.2.4}%
\contentsline {chapter}{\numberline {5}Discussion}{41}{chapter.5}%
\contentsline {section}{\numberline {5.1}Key findings}{41}{section.5.1}%
\contentsline {subsection}{\numberline {5.1.1}Larger models lead to better results}{41}{subsection.5.1.1}%
\contentsline {subsection}{\numberline {5.1.2}Character-level tokenization strategies perform better on tasks which require character-level information content}{42}{subsection.5.1.2}%
\contentsline {subsection}{\numberline {5.1.3}Including annotations improves enumeration accuracy, mean edit distance, and mean semantic similarity, but not accuracy}{42}{subsection.5.1.3}%
\contentsline {subsection}{\numberline {5.1.4}Human perceptions of puzzle difficulty track across all models}{43}{subsection.5.1.4}%
\contentsline {section}{\numberline {5.2}Limitations}{43}{section.5.2}%
\contentsline {section}{\numberline {5.3}Summary}{44}{section.5.3}%
\contentsline {chapter}{\numberline {A}Explanations of each type of clue}{48}{appendix.A}%
\contentsline {chapter}{\numberline {B}Sample Predictions}{50}{appendix.B}%
