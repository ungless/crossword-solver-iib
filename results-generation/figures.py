import matplotlib.pyplot as plt
import numpy as np
import pandas

OUTPUT_DIR = 'figures/'

results = pandas.read_csv('final_results.csv')
            
clue_types = ("Anagram", 'Containment', 'Charade', 'Subtraction', 'Substitution', 'Hidden',
              'Homophone', 'Reversal', 'Cryptic definition', 'Double definition', '&lit')
classes = ('Telegraph', 'Times', 'quick')
models = ('T5 Large', 'T5 3B', 'ByT5 Large', 'ByT5 XL')
model_configurations = ('Annotated, force explanation', 'Annotated, force solution',
                        'Unannotated, force solution')
metrics = (('accuracy',100), ('enumeration_accuracy', 100),('mean_semantic_similarity',1), ('mean_edit_distance', 10))


def by_clue_type(model, configuration_n, ax):
    clue_type_labels = [c + ' (n=' + str(int(results.loc[1][c+'_count'])) + ') ' for c in clue_types]
    values = []
    rows = results.loc[results['Model'] == model].reset_index().loc[configuration_n]

    for c in clue_types:
        label = c + '_accuracy'
        values.append(rows.loc[label]*100)

    y_pos = np.arange(11)
    rects = ax.barh(y_pos, values, align='center')
    ax.bar_label(rects, fmt='%.1f')

    ax.set_xlabel('Accuracy (%)')
    ax.set_title(model + ' ' + model_configurations[configuration_n].lower(), loc='center')
    ax.set_yticks(y_pos, labels=clue_type_labels)
    ax.set_xlim(0, 100)

    return ax

def by_model_and_config(metric, ax):
    values = {}

    for c in model_configurations:
        values[c] = []

    for model in models:
        rows = results.loc[results['Model'] == model].reset_index()
        for i, config in enumerate(model_configurations):
            val = rows.loc[i][metric[1]]
            if metric[2] == 100 or metric[2] == 20:
                val = val * 100
            values[config].append(val)

    x = np.arange(len(models))
    width = 0.2
    multiplier = 0

    for configuration, value in values.items():
        offset = width * multiplier
        rects = ax.bar(x + offset, value, width, label=configuration)
        ax.bar_label(rects,fmt='%.1f', padding=2, rotation="vertical")
        multiplier += 1

    # Add some text for labels, title and custom x-axis tick labels, etc.
    if metric[2] == 100 or metric[2] == 20:
        ax.set_ylabel(metric[0].title()+ '(%)')
    else:
        ax.set_ylabel(metric[0].title())
    ax.set_title(metric[0].title() +' by model')
    ax.set_xticks(x + width, models)
    ax.set_xlabel('Model')
    ax.set_ylim(0, metric[2])
    ax.set_title(metric[3], fontfamily='serif', loc='left', fontsize='large', fontweight='bold')
    
    return ax

def clue_type_by_model(metric, clue_type, ax):
    values = {}
    col = clue_type + '_' + metric[0]

    for c in model_configurations:
        values[c] = []

    for model in models:
        rows = results.loc[results['Model'] == model].reset_index()
        for i, config in enumerate(model_configurations):
            val = rows.loc[i][col]
            if metric[1] == 100 or metric[1] == 20:
                val = val * 100
            values[config].append(val)

    x = np.arange(len(models))
    width = 0.2
    multiplier = 0

    for configuration, value in values.items():
        offset = width * multiplier
        rects = ax.bar(x + offset, value, width, label=configuration)
        ax.bar_label(rects, fmt='%.1f', padding=2, rotation="vertical")
        multiplier += 1


    human_readable_label = metric[0].replace('_', ' ').title()
    if metric[1] == 100 or metric[1] == 20:
        ax.set_ylabel(human_readable_label + '(%)')
    else:
        ax.set_ylabel(human_readable_label)
    ax.set_title(clue_type.title() + ' (n=' + str(int(results.loc[1][clue_type+'_count'])) + ') ' + human_readable_label +' by model')
    ax.set_xticks(x + width, models)
    ax.set_xlabel('Model')
    ax.set_ylim(0, metric[1])
#    ax.set_title(metric[2], fontfamily='serif', loc='left', fontsize='large', fontweight='bold')
    
    return ax
    
plt.style.use('grayscale')
fig1, ((axs1, axs2), (axs3, axs4)) = plt.subplots(ncols=2, nrows=2, figsize=(10,7),
                        sharex=False, sharey=False)

axs1 = by_model_and_config(metric=('accuracy', 'Eval Accuracy', 20, 'a'), ax=axs1)
axs2 = by_model_and_config(metric=('mean edit distance', 'Mean Edit Distance', 10, 'b'), ax=axs2)
axs3 = by_model_and_config(metric=('mean semantic similarity', 'Mean Semantic Similarity', 1, 'c'), ax=axs3)
axs4 = by_model_and_config(metric=('enumeration accuracy', 'Eval Enumeration Accuracy', 100, 'd'), ax=axs4)

handles, labels = axs1.get_legend_handles_labels()
lgd1 = fig1.legend(handles, labels, ncols=3, loc = 'lower center', bbox_to_anchor=(0.5,-0.05))
fig1.subplots_adjust(bottom=0.02)

fig1.tight_layout()

fig2, axs = plt.subplots(ncols=3, nrows=4, figsize=(14, 14),
                         sharex=False, sharey=False)

labels = ['a','b','c','d','e','f','g','h','i','j','k','l','m']
for i, model in enumerate(models):
    for j in range(3):
        axs[i, j] = by_clue_type(model=model, configuration_n=j, ax=axs[i,j])
        axs[i,j].set_title(labels[0], fontfamily='serif', loc='left', x=-0.1, fontsize='large', fontweight='bold')
        labels.pop(0)

#fig2.legend(clue_types, loc='upper center', ncols=1, numpoints=1,bbox_to_anchor=(0.5, 0.05))
fig2.tight_layout()
DPI = 400

for c in clue_types + classes:
    labels = ['a','b','c','d','e','f','g','h','i','j','k','l','m']
    fig, axs = plt.subplots(ncols=2, nrows=2, figsize=(10,7))
    n = 0
    for i in range(2):
        for j in range(2):
            axs[i, j] = clue_type_by_model(metrics[n],c, axs[i,j])
            axs[i,j].set_title(labels[0], fontfamily='serif', loc='left', x=-0.1, fontsize='large', fontweight='bold')
            labels.pop(0)
            n+=1

    handles, labels = axs[0,0].get_legend_handles_labels()
    lgd1 = fig.legend(handles, labels, ncols=3, loc = 'lower center', bbox_to_anchor=(0.5,-0.05))
    fig.subplots_adjust(bottom=0.02)
    fig.tight_layout()
    fig.savefig(OUTPUT_DIR + c + '_by_model.png', bbox_extra_artists=(lgd1,), bbox_inches='tight', dpi=DPI)

    
fig1.savefig(OUTPUT_DIR + 'by_model_and_config.png', dpi=DPI, bbox_extra_artists=(lgd1,), bbox_inches='tight')
fig2.savefig(OUTPUT_DIR + 'by_clue_type.png', dpi=DPI, bbox_inches='tight')
    
