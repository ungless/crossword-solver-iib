import matplotlib.pyplot as plt
import numpy as np
import pandas
from collections import Counter
from collections import defaultdict

df_test = pandas.read_csv('../data/cryptonite-test-annotated.csv')
df_train = pandas.read_json('../data/cryptonite-train.jsonl',
                            lines=True, orient='columns')

def get_length_by_enumeration(enumeration):
    if not isinstance(enumeration, str):
        return (0, 0)
    length = 0
    enumeration = enumeration.replace('(', '').replace(')','')
    nums = enumeration.split(',')
    for n in nums:
        length += int(n)
    return (length, len(nums))

# train
## number of words in answer
times = 0
telegraph = 0
quick = 0
for i, row in df_train.iterrows():
    if row['publisher'] == 'Times':
        times += 1
    elif row['publisher'] == 'Telegraph':
        telegraph += 1

    if row['quick'] == True:
        quick += 1
        
#    length = get_length_by_enumeration(row['enumeration'])
#    length_data[c_type].append(length)
 #   if row['secondary_type']:
#        length_data[row['secondary_type']].append(length)
 #   length_data['overall'].append(length)

print(times, telegraph, quick, len(df_train))

for k,v in length_data.items():
    print(k)
    frequencies = Counter(v)
    for n in range(1,6):
        l = []
        for j in range(3,16):
            l.append(f'\'{str(frequencies[(j, n)])}\'')
        print(','.join(l))

    
    print(frequencies)

# fig, axs = plt.subplots()
# axs = axs.reshape(-1)
# ax.barh(clue_types, counts)
# ax.set(xlim=[0, 300], xlabel='Frequency', ylabel='Clue Type',
#        title='Frequency of each clue type')
# fig.savefig('figures/meta/clue_type_freq.png', bbox_inches='tight', dpi=400)


    
## answer length distribution
## enumeration distribution by word
## clue length distribution by word
## clue length distribution by character
## answer diversity (how many distinct answers are there)

# test
## distribution of annotations clues by primary and secondary type

# fig, ax = plt.subplots(figsize=(12, 2))
# annotation_distributions = []
# for c in clue_types:
#     count = results.loc[1][c+'_count']
#     annotation_distributions.append(count)

# data = np.array(annotation_distributions)
# category_colors = plt.colormaps['winter'](
#         np.linspace(0.15, 0.85, len(annotation_distributions)))

# data_cum = data.cumsum(axis=0)

# for i, (colname, color) in enumerate(zip(clue_types, category_colors)):
#     widths = data[i]
#     starts = data_cum[i] - widths
#     rects = ax.barh('Clue type distribution', widths, left=starts, height=0.5,
#                     label=colname, color=color)

#     r, g, b, _ = color
#     text_color = 'white' if r * g * b < 0.5 else 'darkgrey'
#     ax.bar_label(rects, label_type='center', color=text_color)

# ax.set_xlim(0, np.sum(data, axis=0).max())

# ax.legend(ncols=len(clue_types), bbox_to_anchor=(0, 1),
#               loc='lower left', fontsize='small')

# fig.savefig(OUTPUT_DIR + 'dist_types.png', bbox_inches='tight', dpi=DPI)
