import matplotlib.pyplot as plt
import matplotlib.patches as mpatches
import numpy as np
import pandas
from collections import defaultdict

results = pandas.read_csv('final_results.csv')

clue_types = ("Anagram", 'Charade', 'Containment', 'Cryptic definition',
              'Double definition', 'Hidden', 'Homophone', 'Reversal',
              'Subtraction', 'Substitution', '&lit')

metrics = ('accuracy', 'enumeration_accuracy',
           'mean_edit_distance', 'mean_semantic_similarity')


models = [('2T5-L-annotated-EXP',
'2T5-L-annotated-SOL'),
('2T5-3b-annotated-EXP',
'2T5-3b-annotated-SOL'),
('2ByT5-xl-unannotated-SOL',),
('2T5-3b-unannotated-SOL',),
('2T5-L-unannotated-SOL',),
('2ByT5-L-unannotated-SOL',)]

plt.style.use('grayscale')

bar_colors = ['white', 'black', 'gray']

def get_model_string_from_name(name):
    sections = name.split('-')
    sections[0] = sections[0].replace('2', '')
    out = f'{sections[0]}-{sections[1]}  {sections[2]}, forcing {sections[3].lower()}.'
    return out

# Clue type distribution bars
counts = []
for c in clue_types:
    counts.append(int(results.loc[1][c + '_count']))

print(clue_types, counts)

fig, ax = plt.subplots()
ax.barh(clue_types, counts)
ax.set(xlim=[0, 300], xlabel='Frequency', ylabel='Clue Type',
       title='Frequency of each clue type')
fig.savefig('figures/meta/clue_type_freq.png', bbox_inches='tight', dpi=400)


# Clue type performance by each metric x4 for each model

for model in models:
    if len(model) == 1:
        title = get_model_string_from_name(model[0])
        row = results[results['Model'] == model[0]]
        fig, axs = plt.subplots(ncols=2, nrows=2, sharey=True)
        fig.tight_layout()
        axs = axs.reshape(-1)
        for i, m in enumerate(metrics):
            data = {}
            for c in clue_types:
                value = row[c + '_' + m].iloc[0]
                c = c + ' (n=' + str(row[c + '_count'].iloc[0]) + ')'
                data[c] = value
            axs[i].barh(list(data.keys()), list(data.values()))
#        axs[i].axvline(np.mean(list(data.values())), ls='--', color='r')
            axs[i].set(ylabel='Clue Type',
                   title=m)
    else:
        title = model[0].replace('-EXP', '').replace('-', ' ').replace('2', '')
        row1 = results[results['Model'] == model[0]]
        row2 = results[results['Model'] == model[1]]
        print(row1)
        fig, axs = plt.subplots(ncols=2, nrows=2, sharey=True)
        fig.tight_layout()
        axs = axs.reshape(-1)
        for i, m in enumerate(metrics):
            data = {'Forcing SOL': [],
                    'Forcing EXP': []}
            labels = []
            for c in clue_types:
                value2 = row2[c + '_' + m].iloc[0]
                value1 = row1[c + '_' + m].iloc[0]
                c = c + ' (n=' + str(results[c + '_count'].iloc[0]) + ')'
                labels.append(c)
                data['Forcing SOL'].append(value2)
                data['Forcing EXP'].append(value1)

            x = np.arange(len(clue_types))  # the label locations
            width = 0.4  # the width of the bars
            multiplier = 0
                
            for j, (condition, value) in enumerate(data.items()):
                offset = width * multiplier
                rects = axs[i].barh(x + offset, value, width, label=condition,
                                    color=bar_colors[j],
                                    edgecolor='black')
                multiplier += 1

            axs[i].set_yticks(range(len(clue_types)))
            axs[i].set(yticklabels=labels)
            axs[i].set(ylabel='Clue Type', title=m)
        axs[0].legend(ncols=2,loc='upper center', bbox_to_anchor=(1, 1.5),
                          edgecolor='black')
            
    
    fig.suptitle(title, y=1.05)
    fig.savefig('figures/models/' + model[0] + '_by_clue_type.png', dpi=400,
                bbox_inches='tight')

# Single words: Line graph of increasing enumeration length by each metric x4 for each model
for model in models:
    fig, axs = plt.subplots(ncols=2, nrows=2, sharex=True)
    axs = axs.reshape(-1)
    if len(model) == 1:
        row = results[results['Model'] == model[0]]
    else:
        row1 = results[results['Model'] == model[0]]
        row2 = results[results['Model'] == model[1]]

    if len(model) == 1:
        for i, m in enumerate(metrics):
            data = {}
            for c in range(3, 16):
                value = row[str(c) + '_enum_' + m].iloc[0]
                data[str(c)] = value
            axs[i].plot(list(data.keys()), list(data.values()))
            axs[i].set(ylabel=m, xlabel='Enumeration')
        fig.suptitle(get_model_string_from_name(model[0]), y=1.02)
        fig.tight_layout()

    else:
        for i, m in enumerate(metrics):
            data = [{}, {}]

            for c in range(3, 16):
                value1 = row1[str(c) + '_enum_' + m].iloc[0]
                data[0][str(c)] = value1
                value2 = row2[str(c) + '_enum_' + m].iloc[0]
                data[1][str(c)] = value2

            axs[i].plot(data[0].keys(), data[0].values(), '--', label='Forcing EXP', color='black')
            axs[i].plot(data[0].keys(), data[1].values(), label='Forcing SOL', color='black')
            axs[i].set(ylabel=m, xlabel='Enumeration')

        fig.tight_layout()

        axs[0].legend(ncols=2,loc='upper center', bbox_to_anchor=(1.1, 1.4),
                          edgecolor='black')

        fig.suptitle(model[0].replace('-EXP', '').replace('-', ' ').replace('2', ''), y=1.02)

    fig.savefig('figures/models/' + model[0] + '_by_1word_enumeration.png', dpi=300,
                bbox_inches='tight')

# 1 word vs multiwords x4 metrics for each model

fig, axs = plt.subplots(ncols=3, nrows=1, sharey=True)
fig.tight_layout()
axs = axs.reshape(-1)

for i, m in enumerate(metrics[0:3]):
    data = {'Single word': tuple(results['1word_enum_' + m]),
            'Multi-word': tuple(results['gt_2words_enum_' + m])}
    models = results['Model'].to_list()
    models.sort()
    models = tuple(models)
    x = np.arange(len(models))  # the label locations
    width = 0.25  # the width of the bars
    multiplier = 0
    
    for j, (n_words, value) in enumerate(data.items()):
        offset = width * multiplier
        rects = axs[i].barh(x + offset, value, width, label=n_words,
                           color=bar_colors[j],
                           edgecolor='black')
#        axs[i].bar_label(rects, padding=3, rotation="vertical")
        multiplier += 1
    
    axs[i].set_xlabel(m)
    axs[i].set_yticks(x + width, [m.replace('2', '') for m in models])

axs[0].legend(ncols=2,loc='upper center', bbox_to_anchor=(1.5, 1.1), edgecolor='black')

fig.savefig('figures/meta/words_n_by_model.png', dpi=400,
            bbox_inches='tight')

        
# Performance across all models by each metric x4 for each clue type
conditions = list(clue_types)
for c in conditions:
    fig, axs = plt.subplots(ncols=2, nrows=2, sharey=True)
    fig.tight_layout()
    axs = axs.reshape(-1)

    white_patch = mpatches.Patch(color='white', label='Annotated',
                                 edgecolor='black')
    white_patch.set_edgecolor('black')
    black_patch = mpatches.Patch(color='black', label='Unannotated')

    for i, m in enumerate(metrics):
        values = []
        colours = []
        for j, n in enumerate(models):
            row = results.loc[j]
            values.append(row[c + '_' + m])
            if '-annotated' in n:
                colours.append('white')
            else:
                colours.append('black')
        axs[i].barh(models, values, color=colours, edgecolor='black')
        axs[i].set_xlabel(m)
        axs[i].set_yticks(x+width, [o.replace('2', '') for o in models])


    axs[0].legend(handles=[white_patch, black_patch],
                  loc='upper center',
                  ncols=2, bbox_to_anchor=(1, 1.4), edgecolor='black')
    fig.suptitle(c + ' clues by model', y=1.02)
    fig.savefig('figures/meta/'+ c + '_by_model.png', dpi=400,
                bbox_inches='tight')

classes = ['Times', 'Telegraph', 'quick']
fig, axs = plt.subplots(ncols=2, nrows=2, sharey=True, figsize=(7, 6))
fig.tight_layout()
axs = axs.reshape(-1)

for i, m in enumerate(metrics):
    data = {'Times': [],
            'Telegraph': [],
            'Quick': []}
    
    for model in models:
        row = results[results['Model'] == model]
        for c in classes:
            value = row[str(c) + '_' + m].iloc[0]
            data[c.title()].append(value)

    x = np.arange(len(models))  # the label locations
    width = 0.2  # the width of the bars
    multiplier = 0
                
    for j, (c, value) in enumerate(data.items()):
        offset = width * multiplier
        rects = axs[i].barh(x + offset, value, width, label=c,
                            color=bar_colors[j],
                            edgecolor='black')
        multiplier += 1
    
    axs[i].set(ylabel='Model',yticks=range(len(models)), yticklabels=[o.replace('2','') for o in models], xlabel=m)
    
fig.tight_layout()
    
axs[0].legend(ncols=3,loc='upper center', bbox_to_anchor=(0.6, 1.3),
                              edgecolor='black')
    
fig.suptitle('Model performance by puzzle series', y=1.02)
    
fig.savefig('figures/meta/model_by_clue_class.png', dpi=300,
                   bbox_inches='tight')
    
        
