from os import listdir
from os.path import isfile, join
import pandas

data = pandas.read_csv('crossword-solver-iib/data/cryptonite-test-annotated.csv')
primary_type = data['primary_type']
secondary_type = data['secondary_type']

path = 'eval_output/predictions/'
files = [f for f in listdir(path) if isfile(join(path,f))]

for i, f in enumerate(files):
    df = pandas.read_csv(path+f)
    df['primary_type'] = primary_type
    df['secondary_type'] = secondary_type
    df.to_csv(path+'2'+f)


