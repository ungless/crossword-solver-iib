import pandas                                                                                
import regex as re                                                                           
from tqdm import tqdm

CAPS_PATTERN = r'([A-Z-:])+$'
DATA_PATH = 'crossword-solver-iib/data/annotated-dataset.jsonl'
OUTPUT = 'crossword-solver-iib/data/annotated-dataset2.jsonl'
dataset = pandas.read_json(DATA_PATH, lines=True, orient='records')

for i, row in enumerate(tqdm(dataset['explanation'])):
    if row:
        dataset.loc[i, 'explanation'] = row.replace('\u2013 ', '').strip().encode('utf-8')

print(dataset['explanation'])
    
dataset.to_json(OUTPUT, lines=True, orient='records', mode='w', force_ascii=False)
