from tqdm import tqdm
import pandas

annotated_dataset = pandas.read_json('crossword-solver-iib/blog-scraper/data/clue_annotations.jsonl', lines=True, orient='columns')

annotated_dataset = annotated_dataset.drop_duplicates(subset='clue')

test_dataset = pandas.read_json('crossword-solver-iib/efrat-replication/data/cryptonite-test.jsonl', lines=True, orient='columns')
train_dataset = pandas.read_json('crossword-solver-iib/efrat-replication/data/cryptonite-train.jsonl', lines=True, orient='columns')

train_dataset['clue_lower'] = train_dataset['clue'].str.lower()
test_dataset['clue_lower'] = test_dataset['clue'].str.lower()
#test_dataset['annotated'] = False
annotated_dataset['clue_lower'] = annotated_dataset['clue'].str.lower()
#annotated_dataset['annotated'] = True

df_merged = pandas.merge(annotated_dataset, test_dataset, how='outer')
df_merged['dup'] = df_merged.duplicated(subset='clue_lower', keep=False)

duplicates = df_merged.loc[df_merged['dup'] == True]

for i, clue1 in enumerate(tqdm(duplicates['clue_lower'])):
    for j, clue2 in enumerate(duplicates['clue_lower']):
        if clue1 == clue2:
            annotated_dataset.drop(annotated_dataset.loc[annotated_dataset['clue_lower'] == clue2].index)
        
import pdb; pdb.set_trace()


