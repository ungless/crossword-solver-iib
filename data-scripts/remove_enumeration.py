import pandas
import regex as re
from tqdm import tqdm

ENUMERATION_PATTERN = r'\ \(([0-9],?\-?)+\)'
TRAIN_PATH = 'crossword-solver-iib/efrat-replication/data/cryptonite-train.jsonl'
TEST_PATH = 'crossword-solver-iib/efrat-replication/data/cryptonite-test.jsonl'

train_dataset = pandas.read_json(TRAIN_PATH, lines=True, orient='columns')

test_dataset = pandas.read_json(TEST_PATH, lines=True, orient='columns')

for i, row in enumerate(tqdm(train_dataset['clue'])):
    train_dataset.loc[i, 'clue'] = re.sub(ENUMERATION_PATTERN, '', row)
    
for i, row in enumerate(tqdm(test_dataset['clue'])):
    test_dataset.loc[i, 'clue'] = re.sub(ENUMERATION_PATTERN, '', row)

train_dataset.to_json(TRAIN_PATH, lines=True, orient='records', mode='w')
test_dataset.to_json(TEST_PATH, lines=True, orient='records', mode='w')
