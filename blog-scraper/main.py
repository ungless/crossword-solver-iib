import sys
import argparse
import regex as re
import bs4
import requests
import json
import time
import logging

OUTPUT_FILE = 'data/clue_annotations.jsonl'
log_file = 'scraper.log'

TFT_CRYPTIC_BASE_URL = "https://timesforthetimes.co.uk/category/daily-cryptic/page/"
TFT_QUICK_CRYPTIC_BASE_URL = "https://timesforthetimes.co.uk/category/quick-cryptic/page/"

BD_BASE_URL = "https://bigdave44.com/tag/daily-telegraph/page/"
ENUMERATION_PATTERN = r'\ \(([0-9],?\-?)+\)'
BD_EXPLANATION_PATTERN = r'(?<=[A-Z]{3,}[\s:}](\s-)?)(.+)'

def get_orientation(i):
    if i == 0:
        return "across"
    elif i == 1:
        return "down"
    else:
        return "unknown"

def get_tft_page_set(index):
    r = requests.get(TFT_CRYPTIC_BASE_URL+str(index))
    output = r.text
    page_set = []

    soup = bs4.BeautifulSoup(output, "html.parser")
    tags = soup.select("footer.tftt-entry-footer a")
    links = [l.get('href') for l in tags if 'comments' not in l.get('href')]
    logging.info(f'Requesting TFT page set: {links}')
    
    for link in links:
#        print("Requesting", link)
        r = requests.get(link)
        time.sleep(1)

        output = r.text
        page_set.append(output)

    return [links, page_set]

def parse_tft_page(page):
    link = page[0]
    page_text = page[1]
    soup = bs4.BeautifulSoup(page_text, "html.parser")
    tbody_tags = soup.find_all("tbody")

    if len(tbody_tags) <= 2:
        tbody_tags = soup.find_all("div.entry-content")
        is_paragraphed = True
    else:
        tbody_tags = tbody_tags[:2]
        is_paragraphed = False
    all_clues = []
    for j, section in enumerate(tbody_tags):
        clues = []
        if is_paragraphed:
            tr_tags = section.find_all("p")
        else:
            tr_tags = section.find_all("tr")
        clue_tags = tr_tags[1::2]
        solution_tags = tr_tags[::2]
        if len(solution_tags) <= 1 or solution_tags[1].select_one("b, strong") == None:
            logging.warning(f'{link}: Skipping entry because no solution(s) can be found')
            continue
        is_separate_solution = solution_tags[1].select_one("b,strong").parent.find(text=True, recursive=False) == None

        if is_separate_solution:
            clue_tags = tr_tags[1::3]
            solution_tags = tr_tags[2::3]
            explanation_tags = tr_tags[3::3]

            if len(explanation_tags) != len(solution_tags):
                logging.warning(f'{link}: Skipping page half because misalignment of explanation and solution tags')
                continue

        for tag in clue_tags:
            if is_separate_solution:
                definition_tags = tag.select("u")
            else:
                definition_tags = tag.select("span[style*='underline'], u")
            definitions = [d.get_text().strip().replace(']', '').replace('[','') for d in definition_tags]

            if len(definition_tags) == 0:
                logging.warning(f'{link}: Skipping tag as no definition found in {tag}')
                continue
            try:
                wordplay_tag = definition_tags[0].find_parent("td")
            except IndexError:
                logging.warning(f'{link}: Skipping tag as Wordplay tag not found in tag {tag} with definition tag {definition_tags[0]}')
                continue

            if not wordplay_tag:
                wordplay_tag = definition_tags[0].find_parent("span")

            if True:
                wordplay = wordplay_tag.get_text()
                for d in definitions:
                    wordplay = wordplay.replace(d, '')

                wordplay = re.sub(ENUMERATION_PATTERN, '', wordplay).replace(']', '').replace('[','').strip()

                whole_clue = wordplay_tag.get_text(strip=True, separator=' ').replace(' ?', '?').replace(']', '').replace('[','').replace('/','').replace(' :', ':').replace(' ,', ',').replace(' ‘', '‘').replace(' !', '!')
                try:
                    enumeration = re.search(ENUMERATION_PATTERN, whole_clue).group(0).replace(' ', '')
                except AttributeError:
                    logging.warning(f'{link}: Skipping tag as enumeration not found in tag {tag}')
                    continue
                whole_clue = re.sub(ENUMERATION_PATTERN, '', whole_clue)

                clue = {"definitions": definitions,
                        "wordplay": wordplay,
                        "clue": whole_clue,
                        "enumeration": enumeration,
                        "orientation": get_orientation(j),
                        "publisher": "Times"}
                clues.append(clue)

        for i, tag in enumerate(solution_tags):
            solution_tag = tag.select_one("b")

            if solution_tag:
                if is_separate_solution:
                    explanation = explanation_tags[i].get_text(strip=True)
                else:
                    explanation = solution_tag.find_parent("td").find(text=True, recursive=False)
                    if explanation == '' or explanation == None:
                        continue
                    else:
                        explanation = explanation.strip()
                    i = i - 1

                explanation = explanation.replace('\n', '').replace('-', '').replace('–', '').strip()
                solution = solution_tag.get_text().strip()

                try:
                    clues[i]['explanation'] = explanation
                except IndexError:
                    logging.warning(f'Missing definition/wordplay in {explanation}')
                    continue
                clues[i]['solution'] = solution.lower()

        all_clues.extend(clues)
    logging.info(f'Processed {len(all_clues)} clues on TFT page {link}')
    return all_clues

def get_bd_page_set(index):
    r = requests.get(BD_BASE_URL+str(index))
    output = r.text
    page_set = []
    soup = bs4.BeautifulSoup(output, "html.parser")
    tags = soup.select("h2.entry-title a")
    links = [l.get('href') for l in tags if 'hints' not in l.get_text().lower() and 'toughie' not in l.get_text().lower()]
    logging.info(f'Requesting BD page set: {links}')
    for link in links:
#        print("Requesting", link)
        r = requests.get(link)
        time.sleep(1)

        output = r.text
        page_set.append(output)

    return [links, page_set]

def parse_bd_page(page):
    link = page[0]
    page_text = page[1]
    
    soup = bs4.BeautifulSoup(page_text, "html.parser")
    content = soup.select_one(".entry-content")
    answer_tags = content.select("span.spoiler")
    clues = []

    if len(answer_tags) == 0:
      entry_tags = content.select("p:not([style])")

      for tag in entry_tags:
          tag_text = tag.get_text().replace(u'\xa0', u' ')
          if 'Across' in tag_text or 'Down' in tag_text:
              continue
          elif tag_text == ' ' or tag_text == '':
              continue

          try:
              answer_text = re.search(r'([A-Z]+[-\ ]*)((?=([:—–}]))|(?=[\ ]{8}))', tag_text).group(0).strip()
          except AttributeError:
              logging.warning(f'{link}: Skipping tag: no answer text found in BD tag, {tag_text}')
              continue
          try:
              explanation_text = re.search(BD_EXPLANATION_PATTERN, tag_text).group(0).strip()
          except AttributeError:
              logging.warning(f'{link}: Skipping tag: no explanation text found in BD tag, {tag_text}')
              continue              
          definition_tags = tag.select("span[style*='underline'], u, .mrkUnderS, .dls")
          
          definition_texts = [d.get_text().strip() for d in definition_tags]
          try:
              enumeration = re.search(ENUMERATION_PATTERN, tag_text).group(0).replace(' ', '')
          except AttributeError:
              logging.warning(f'{link}: Skipping tag as no enumeration found in {tag_text}')
              continue
          try:
              clue_number = re.search(r'([0-9]+[ad])', tag_text).group(0)
          except AttributeError:
              logging.warning(f'{link}: Skipping tag as no clue_number found in {tag_text}')
              continue
          
          orientation = ['a', 'd'].index(re.search(r'([ad])', clue_number).group(0))

          whole_clue = tag_text.replace(enumeration, '').replace(clue_number, '').replace(explanation_text, '').replace(answer_text, '').replace(' ?', '?').replace(']', '').replace('[','').replace('/','').replace(' :', ':').replace(' ,', ',').replace(' ‘', '‘').replace(' !', '!').replace('\n','').replace(u'\xa0', u'').replace('}', '').replace('{', '').strip()
          wordplay_text = whole_clue
          for d in definition_texts:
              wordplay_text = wordplay_text.replace(d, '')

          explanation_text = explanation_text.replace(':', '').strip()
          clue = {"definitions": definition_texts,
                                "wordplay": wordplay_text.strip(),
                                "clue": whole_clue,
                                "answer": answer_text.lower(),
                                "explanation": explanation_text,
                                "enumeration": enumeration,
                                "orientation": get_orientation(orientation),
                                "publisher": "Telegraph"}
#          print(clue)
          clues.append(clue)

    else:
        for tag in answer_tags:
            # check if its the quickie pun
            if len(tag.parent.select("span.spoiler")) > 2:
                continue
            try:
                answer_text = tag['rel']
            except KeyError:
                answer_text = tag.get_text().strip()
            parent_tag = tag.find_parent("p")

            if not parent_tag:
                logging.warning(f'{link}: Skipping tag, parent tag not valid in {tag}')
                continue
            
            try:
                explanation_text = re.search(BD_EXPLANATION_PATTERN, parent_tag.get_text()).group(0).strip()
            except AttributeError:
                logging.warning(f'{link}: Skipping tag, no BD explanation text found in {parent_tag.get_text()}')
                continue
#            explanation_text = [c for c in parent_tag.contents if type(c) == bs4.element.NavigableString][-1].strip()

            definition_tags = parent_tag.select("span[style*='underline'], u, .mrkUnderS, .dls")
            
            if len(definition_tags) == 0:
                clue_entry_tag = parent_tag.previous_sibling
                if type(clue_entry_tag) == bs4.element.NavigableString:
                    continue
                all_entry_text = clue_entry_tag.get_text() + parent_tag.get_text()
                definition_tags = clue_entry_tag.select("span[style*='underline'], u, .mrkUnderS, .dls")
            else:
                all_entry_text = parent_tag.get_text()
                if all_entry_text == None:
                    continue
            definition_texts = [d.get_text().strip() for d in definition_tags]

            all_entry_text = all_entry_text.replace(explanation_text, '').replace(answer_text, '').replace('\n', '').replace(u'\xa0', u' ')
            try:
                enumeration = re.search(ENUMERATION_PATTERN, all_entry_text).group(0).replace(' ', '')
            except AttributeError:
                logging.warning(f'{link}: Skipping tag, no enumeration found in {all_entry_text}')
                continue

            try:
                clue_number = re.search(r'([0-9]+[ad])', all_entry_text).group(0)
            except:
                logging.warning(f'{link}: Skipping tag, no clue_number found in {all_entry_text}')
                continue
            orientation = ['a', 'd'].index(re.search(r'([ad])', clue_number).group(0))

            wordplay_text = all_entry_text.replace(clue_number, '').replace(enumeration, '').replace('\n', '').replace(u'\xa0', u'').replace(' ?', '?').replace(']', '').replace('[','').replace('/','').replace(' :', ':').replace(' ,', ',').replace(' ‘', '‘').replace(' !', '!').strip()
            for d in definition_texts:
                wordplay_text = wordplay_text.replace(d, '')
            
            whole_clue = parent_tag.get_text().replace(enumeration, '').replace(clue_number, '').replace(explanation_text, '').replace(answer_text, '').replace(' ?', '?').replace(']', '').replace('[','').replace('/','').replace(' :', ':').replace(' ,', ',').replace(' ‘', '‘').replace(' !', '!').replace('\n','').replace(u'\xa0', u'').strip()

            explanation_text = explanation_text.replace(':', '').strip()
            clue = {"definitions": definition_texts,
                    "wordplay": wordplay_text,
                    "clue": whole_clue,
                    "answer": answer_text.lower(),
                    "explanation": explanation_text,
                    "enumeration": enumeration,
                    "orientation": get_orientation(orientation),
                    "publisher": "Telegraph"}
            
            clues.append(clue)
    
    logging.info(f'Processed {len(clues)} clues on BD page {link}')
    return clues

def run_scraper(blog, min_page, up_to_page):
    logging.info(f'Running scraper on {blog.upper()} indexes between {min_page} and {up_to_page}')
    file = open(OUTPUT_FILE, 'a+', encoding='utf-8')
    for i in range(min_page, up_to_page+1):
        logging.info(f'Requesting index page set {i}')
        if blog == "tft":
            page_set = get_tft_page_set(i)
        elif blog == "bd":
            page_set = get_bd_page_set(i)
        else:
            return
        for i in range(len(page_set[0])):
            page = [page_set[0][i], page_set[1][i]]
            if blog == "tft":
                clues = parse_tft_page(page)
            elif blog == "bd":
                clues = parse_bd_page(page)
            for clue in clues:
                file.write(json.dumps(clue, ensure_ascii=False) + '\n')
    file.close()

# TFT Up to 201 are OK
# BD Up to 843 are ok

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("--blog")
    parser.add_argument("--min", type=int)
    parser.add_argument("--max", type=int)
    parser.add_argument("--log-file")
    args = parser.parse_args()
    if args.log_file:
        log_file = args.log_file
    logging.basicConfig(handlers=[
        logging.StreamHandler(sys.stdout),
        logging.FileHandler(log_file, mode="a"),
    ],
                        format='%(asctime)s %(levelname)s %(message)s',
                        datefmt='%m/%d/%Y %I:%M:%S %p',
                        encoding='utf-8',
                        level=logging.INFO)

    run_scraper(args.blog, args.min, args.max)
