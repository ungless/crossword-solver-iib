import argparse
import editdistance
import pandas
import numpy as np
from datasets import Dataset
from transformers import AutoTokenizer, AutoModelForSeq2SeqLM, Seq2SeqTrainer, Seq2SeqTrainingArguments, GenerationConfig
import torch
from torch.utils.data import DataLoader
from tqdm import tqdm
from transformers import DataCollatorForSeq2Seq
from peft import LoraConfig, TaskType, get_peft_model
import nltk
from nltk.corpus import wordnet as wn
from os import listdir
from os.path import isfile, join

nltk.download('wordnet')

SOLUTION_SPECIAL_TOKEN = '<solution>'
EXPLANATION_SPECIAL_TOKEN = '<explanation>'
WORDPLAY_SPECIAL_TOKEN = '<wordplay>'
DEFINITION_SPECIAL_TOKEN = '<definition>'

TASK_PREFIX = 'solve: '

EVAL_OUTPUT_DIRECTORY = 'eval_output/predictions/'

def get_annotated_dataset():
    df_annotated = pandas.read_json('crossword-solver-iib/data/annotated-dataset2.jsonl', lines=True, orient='columns')
    df_annotated = df_annotated[["clue", "answer", "enumeration", "definitions", "wordplay", "explanation"]]
    df_annotated['input'] = None
    df_annotated['target'] = None
    return df_annotated

def get_datasets():
    df_train = pandas.read_json('crossword-solver-iib/data/cryptonite-train.jsonl', lines=True, orient='columns')
    df_train = df_train[["clue", "answer", 'enumeration']]
    df_train['input'] = None
    df_train['target'] = None
    df_train['explanation'] = None
    df_train['definitions'] = None
    df_train['wordplay'] = None
    
#    df_test = pandas.read_json('crossword-solver-iib/data/cryptonite-test.jsonl', lines=True, orient='columns')
#    df_test = df_test[['clue', 'answer', 'enumeration']]
    df_test = pandas.read_csv('crossword-solver-iib/data/cryptonite-test-annotated.csv')
    df_test = df_test[["clue", "answer", "enumeration", 'quick', 'publisher', 'orientation', 'date', 'primary_type', 'secondary_type']]
    df_test['quick'] = df_test['quick'].fillna(False)
    df_test['date'] = df_test['date'].fillna(0)
    df_test = df_test.fillna('')
    df_annotated = get_annotated_dataset()
    df_train = pandas.concat([df_train, df_annotated], sort=False)
    df_train['answer'] = df_train['answer'].fillna('')

    dataset_train = Dataset.from_pandas(df_train)
    dataset_test = Dataset.from_pandas(df_test[:10])

    return dataset_train, dataset_test

def preprocess(examples, tokenizer):
    inputs = [TASK_PREFIX + clue.lower() + ' ' + enumeration for clue, enumeration in zip(examples["clue"], examples['enumeration'])]
    
    answers = [SOLUTION_SPECIAL_TOKEN + answer for answer in examples['answer']]

    model_inputs = tokenizer(inputs, text_target=answers, max_length=256, truncation=True, padding="longest", return_tensors="pt")
    return model_inputs

def preprocess_with_annotated(examples, tokenizer):
    j = 0
    for i, entry in enumerate(examples['target']):
        if examples['explanation'][i] and examples['answer'][i]:
            examples["target"][i] = EXPLANATION_SPECIAL_TOKEN + examples['explanation'][i][0:400] + SOLUTION_SPECIAL_TOKEN + examples["answer"][i]
        elif examples['answer'][i] != None:
            examples['target'][i] = SOLUTION_SPECIAL_TOKEN + examples['answer'][i]
        else:
            examples['target'][i] = SOLUTION_SPECIAL_TOKEN
        if examples['definitions'][i]:
            j += 1
            clue = examples['clue'][i]
            for d in examples['definitions'][i]:
                location = clue.find(d)
                clue_list = list(clue)
                clue_list.insert(location, DEFINITION_SPECIAL_TOKEN)
                clue = ''.join(clue_list)
            if examples["wordplay"][i]:
                wordplay = examples['wordplay'][i]
                location = clue.find(wordplay)
                clue_list = list(clue)
                clue_list.insert(location, WORDPLAY_SPECIAL_TOKEN)
                clue = ''.join(clue_list)
            if j % 2 == 0: # remove 50% of clues with annotated data
                clue = examples['clue'][i] 
            examples['input'][i] = clue.lower() + ' ' + examples['enumeration'][i]
        elif examples['clue'][i]:
            examples['input'][i] = examples['clue'][i].lower() + ' ' + examples['enumeration'][i]
        else:
            examples['input'][i] = ''
    
    inputs = [TASK_PREFIX + e for e in examples['input']]
    model_inputs = tokenizer(inputs, text_target=examples['target'], max_length=256, padding="longest", truncation=True, return_tensors="pt")
    return model_inputs


def get_accuracy(predictions, labels):
    correct = 0
    enumeration_correct = 0
    for i, prediction in enumerate(predictions):
        if prediction == labels[i]:
            correct += 1
        if len(prediction) == len(labels[i]):
            enumeration_correct += 1
    
    accuracy = correct / len(predictions)
    enumeration_accuracy = enumeration_correct / len(predictions)

    return accuracy, enumeration_accuracy

def get_edit_distances(predictions, labels):
    distances = []
    for i, prediction in enumerate(predictions):
        distance = editdistance.eval(labels[i], prediction)
        distances.append(distance)

    return distances

def get_semantic_similarity(w1, w2):
    try:
        s1 = wn.synsets(w1)
        s2 = wn.synsets(w2)
    except:
        return np.nan
    max_similarity = 0

    for i, s in enumerate(s1):
        for j, z in enumerate(s2):
            sim = s.wup_similarity(z)
            if sim > max_similarity:
                max_similarity = sim

    return max_similarity

def get_dict_as_csv(d):
    headers = []
    output = []
    for key, value in sorted(d.items()):
        for key2, value2 in sorted(value.items()):
            headers.append(key + '_' + key2)
            output.append(str(value2))
    headers_csv = ','.join(headers)
    return headers_csv, ','.join(output)

def get_metrics_from_rows(rows):
    results = {}
    
    answers = rows[rows['answer'].apply(lambda x: isinstance(x, str))]['answer'].tolist()
    predictions_list = rows[rows['predicted_solution'].apply(lambda x: isinstance(x, str))]['predicted_solution'].tolist()
    results = {'accuracy': 0, 'enumeration_accuracy': 0, 'mean_edit_distance': 0, 'mean_semantic_similarity': 0, 'count': 0}
    accuracy, enumeration_accuracy = get_accuracy(predictions_list, answers)
    results['accuracy'] = accuracy
    results['enumeration_accuracy'] = enumeration_accuracy
    results['mean_edit_distance'] = np.mean(get_edit_distances(predictions_list, answers))
    semantic_similarities = [get_semantic_similarity(w1, answers[i]) for i, w1 in enumerate(predictions_list)] 
    results['mean_semantic_similarity'] = np.nanmean(semantic_similarities)
    results['count'] = len(rows)

    return results

def post_eval(pred_output_file):
    results = {}
    predictions = pandas.read_csv(EVAL_OUTPUT_DIRECTORY + pred_output_file).fillna('')
    clue_types = list(set(predictions['primary_type'].values))
    clue_types.extend(['quick', 'Times', 'Telegraph'])
    clue_types.remove('')
    
    for c in clue_types:
        if c == 'quick':
            rows = predictions.loc[predictions['quick'] == True]
        elif c == 'non_quick':
            rows = predictions.loc[predictions['quick'] == False]
        elif c == 'Times':
            rows = predictions.loc[predictions['publisher'] == 'Times']
        elif c == 'Telegraph':
            rows = predictions.loc[predictions['publisher'] == 'Telegraph']
        else:
            rows = predictions.loc[(predictions['primary_type'] == c) | (predictions['secondary_type'] == c)]
        if len(rows) == 0:
            continue
        results[c] = get_metrics_from_rows(rows)
        
    rows = predictions[predictions['answer'].apply(lambda x: len(x.split(' ')) == 1)]
    
    label = '1word_enum'
    results[label] = get_metrics_from_rows(rows)

    n = 3
    while n <= 15:
        rows1 = rows[rows['answer'].apply(lambda x: len(x) == n)]
        print(n, rows1)
        results[str(n) + '_enum'] = get_metrics_from_rows(rows1)
        n += 1    

    rows = predictions[predictions['answer'].apply(lambda x: len(x.split(' ')) > 1)]
    label = 'gt_2words_enum'
    results[label] = get_metrics_from_rows(rows)

    results['overall'] = get_metrics_from_rows(predictions)
    
    print(get_dict_as_csv(results))
    return results

def prepare_compute_metrics(tokenizer, test_dataset, output_file, is_eval):
    def compute_metrics(eval_pred):
        predictions, labels = eval_pred
        predictions = np.where(predictions != -100, predictions, tokenizer.pad_token_id)
        decoded_preds = tokenizer.batch_decode(predictions, skip_special_tokens=False)

        labels = np.where(labels != -100, labels, tokenizer.pad_token_id)
        # might not need to decode everytime. Compare encoded values for accuracy instead?
        decoded_labels = tokenizer.batch_decode(labels, skip_special_tokens=True)
        print(decoded_preds[:50])
        pred_explanations = []
        for i, p in enumerate(decoded_preds):
            try:
                decoded_preds[i] = p.split(SOLUTION_SPECIAL_TOKEN)[1].replace('</s>', '').replace('<pad>', '').strip()
                explanation = p.split(SOLUTION_SPECIAL_TOKEN)[0].replace('</s>', '').replace('<pad>', '').replace('<explanation>', '').strip()
                pred_explanations.append(explanation)
            except:
                decoded_preds[i] = ''
                pred_explanations.append('')
                
        print(decoded_labels[:100], "\n", decoded_preds[:100])
        accuracy, enumeration_accuracy = get_accuracy(decoded_preds, decoded_labels)
        edit_distances = get_edit_distances(decoded_preds, decoded_labels)
        semantic_similarities = [get_semantic_similarity(w1, decoded_labels[i]) for i, w1 in enumerate(decoded_preds)]
        mean_edit_distance = np.mean(edit_distances)
        mean_semantic_similarity = np.nanmean(semantic_similarities)
        

        df_dataset = test_dataset.to_pandas()
        df_dataset['predicted_solution'] = decoded_preds
        df_dataset['predicted_explanation'] = pred_explanations
        df_dataset['solution_edit_distance'] = edit_distances
        df_dataset['semantic_similarity'] = semantic_similarities
        del df_dataset['input_ids']
        del df_dataset['labels']
        del df_dataset['attention_mask']

        if is_eval:
            df_dataset.to_csv('eval_output/predictions/' + output_file)

        return {"accuracy": accuracy, "enumeration_accuracy": enumeration_accuracy, "mean_edit_distance": mean_edit_distance, "mean_semantic_similarity": mean_semantic_similarity}
    return compute_metrics

def get_and_load_model(checkpoint, forced_token=None):
    tokenizer = AutoTokenizer.from_pretrained(checkpoint)
    special_tokens = [SOLUTION_SPECIAL_TOKEN, EXPLANATION_SPECIAL_TOKEN, WORDPLAY_SPECIAL_TOKEN, DEFINITION_SPECIAL_TOKEN]
    tokenizer.add_tokens(special_tokens, special_tokens=True)
    forced_token_id = None
    if forced_token == "explanation":
        forced_token_id = tokenizer.convert_tokens_to_ids(EXPLANATION_SPECIAL_TOKEN)
    elif forced_token == "solution":
        forced_token_id = tokenizer.convert_tokens_to_ids(SOLUTION_SPECIAL_TOKEN)
    
    generation_config = GenerationConfig(num_beams=5,
                                         decoder_start_token_id=tokenizer.pad_token_id,
                                         pad_token_id=tokenizer.pad_token_id,
                                         forced_bos_token_id=forced_token_id,
                                         eos_token_id=tokenizer.eos_token_id,
                                         max_new_tokens=175)
    model = AutoModelForSeq2SeqLM.from_pretrained(checkpoint)
    model.generation_config = generation_config
    model.resize_token_embeddings(len(tokenizer))
    device = torch.device("cuda") if torch.cuda.is_available() else torch.device("cpu")
    model.to(device)
    return model, tokenizer

def get_trainer(checkpoint, pred_output_file, is_eval=False, is_annotated=False, forced_token=None, output_dir="cryptic_model_default"):
    model, tokenizer = get_and_load_model(checkpoint, forced_token=forced_token)

    if not is_eval:
        peft_config = LoraConfig(task_type=TaskType.SEQ_2_SEQ_LM, inference_mode=False, r=32, lora_alpha=32, lora_dropout=0.1,
                                 target_modules=["SelfAttention.q", "SelfAttention.k", "SelfAttention.v", "SelfAttention.o", "EncDecAttention.q", "EncDecAttention.k", "EncDecAttention.v", "EncDecAttention.o", "DenseReluDense.wi", "DenseReluDense.wo"])
        model = get_peft_model(model, peft_config)
        model.print_trainable_parameters()

    data_collator = DataCollatorForSeq2Seq(tokenizer=tokenizer, model=model, return_tensors="pt")

    if not is_eval:
        for n, p in model.named_parameters():
            print(f'{n}: {p.requires_grad}')

    model.enable_input_require_grads()
    model.gradient_checkpointing_enable(gradient_checkpointing_kwargs={"use_reentrant": False})

    dataset_train, dataset_test = get_datasets()
    tokenized_dataset_test = dataset_test.map(preprocess, batched=True, fn_kwargs={"tokenizer": tokenizer})
    if not is_eval:
        print(is_annotated)
        if is_annotated:
            tokenized_dataset_train = dataset_train.map(preprocess_with_annotated, batched=True, fn_kwargs={ "tokenizer": tokenizer})
        else:
            tokenized_dataset_train = dataset_train.map(preprocess, batched=True, fn_kwargs={ "tokenizer": tokenizer})

    training_args = Seq2SeqTrainingArguments(
        output_dir=output_dir,
        evaluation_strategy="epoch",
        predict_with_generate=True,
        learning_rate=2e-4,
        per_device_train_batch_size=32,
        gradient_accumulation_steps=4,
        gradient_checkpointing=True,
        per_device_eval_batch_size=16,
        eval_accumulation_steps=4,
        weight_decay=0.01,
        save_total_limit=3,
        num_train_epochs=4,
#        generation_config=generation_config,
        bf16=True,
#        bf16_full_eval=True,
        push_to_hub=False,
        overwrite_output_dir=False
    )

    compute_metrics = prepare_compute_metrics(tokenizer, tokenized_dataset_test, pred_output_file, is_eval)

    trainer = Seq2SeqTrainer(
        model=model,
        args=training_args,
        train_dataset=tokenized_dataset_train if not is_eval else None,
        eval_dataset=tokenized_dataset_test,
        data_collator=data_collator,
        tokenizer=tokenizer,
        compute_metrics=compute_metrics,
    )
    return trainer

if __name__ == "__main__":
    parser = argparse.ArgumentParser(
                        prog='Efrat replication')

    parser.add_argument('operation', default="evaluate")
    parser.add_argument("--checkpoint")
    parser.add_argument("--annotated", action=argparse.BooleanOptionalAction)
    parser.add_argument("--force", default="none")
    parser.add_argument("--output_dir")
    parser.add_argument("--pred_output")
    args = parser.parse_args()
    if args.operation == "evaluate":
        trainer = get_trainer(args.checkpoint,
                              is_eval=True,
                              forced_token=args.force,
                              pred_output_file=args.pred_output)
        pred = trainer.evaluate()
        print(pred)
        post_eval_results = post_eval(pred_output_file=args.pred_output)
        print(post_eval_results)
    elif args.operation == "post_all":
        path = 'eval_output/predictions/'
        files = [f for f in listdir(path) if (isfile(join(path, f))) and (f[0] == '2')]

        output_csv = open('eval_output/all.csv', 'w+')
        for i, file_name in enumerate(files):
            print(file_name)
            post_eval_results = post_eval(pred_output_file=file_name)

            headers, output = get_dict_as_csv(post_eval_results)
            headers = 'Model,' + headers + '\n'
            if i == 0:
                output_csv.write(headers)
            model_name = file_name.replace(path, '').replace('.csv', '')
            output_csv.write(model_name + ',' + output + '\n')

        output_csv.close()

        
    elif args.operation == "train":
        trainer = get_trainer(args.checkpoint,
                              is_annotated=args.annotated,
                              forced_token=args.force,
                              output_dir=args.output_dir,
                              pred_output_file=args.pred_output)
        print("Training")
        trainer.train()
        trainer.save_model()
    elif args.operation == "inference":
        model, tokenizer = get_and_load_model(args.checkpoint, forced_token=args.force)

        while True:
            clue = input("Clue >>> \n")
            encoded_clue = tokenizer(clue, return_tensors="pt").input_ids
            inputs = encoded_clue.to("cuda:0")

            outputs = model.generate(inputs, decoder_start_token_id=tokenizer.pad_token_id, forced_bos_token_id=tokenizer.convert_tokens_to_ids(EXPLANATION_SPECIAL_TOKEN), eos_token_id=tokenizer.eos_token_id, num_beams=5, max_new_tokens=400)

            print(tokenizer.batch_decode(outputs, skip_special_tokens=False))
