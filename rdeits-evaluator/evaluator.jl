using Test
using CrypticCrosswords
using CrypticCrosswords: definition

import JSON

@testset "Cryptonite" begin
    data = JSON.parsefile("../crosswords/data/cryptonite-final.json")
    println("Loaded test set")
    
    for clue in first(data, 15)
        println("solving\n", clue)
        enumeration = parse(Int, strip(clue["enumeration"], ['(',')']))
        expected_wordplay = clue["answer"]
        solutions, state = solve(replace(clue["clue"], r"\((.*?)\)"im => ""), length=enumeration)
        arc = first(solutions)
        println(arc)
        #        @test definition(arc) == expected_definition || endswith(expected_definition, definition(arc))
        @test arc.output == expected_wordplay
    end
end
